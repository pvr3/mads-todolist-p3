

## Corrección de la práctica

### Documentación

- Muy bien, todo explicado con mucho detalle. Muy buen trabajo.

### Código e implementación

- Funciona todo correctamente
- Muy bien las restricciones de acceso a los controllers
- Muy bien los nuevos métodos de las clases modelo
- Muy bien el código, limpio y documentado


### Calificación

Todo perfecto, enhorabuena.

Calificación: **0,6**  

11 de diciembre de 2013  
Domingo Gallardo

------

#Practica 3 - Múltiples usuarios
##Alumno: Pablo Verdú Romero

###1. Objetivos de la practica.

El objetivo principal de la practica, era el de dotar a la aplicacion la posibilidad de que sea utilizada por varios usuarios, empleando para ello, la identificacion con variables de sesion de Play Framework. Para ello se debian de realizar los cambios necesarios en la aplicacion, en distintos pasos incrementales, para que satifaciera los siguientes requisitos:

1. Pedir un login de usuario y gestionar las tareas del usuario logueado.

2. Permitir el registro de un nuevo usuario.

En el siguiente informe se encuentran detallados el desarrollo incremental que ha sufrido la aplicacion hasta tener completados los anteriormente citados requisitos.

La aplicación final se encuentra alojada en [este](http://mads-todolist.herokuapp.com) enlace de  *Heroku*.


###2. Pasos incrementales del desarrollo.

####2.1 Añadir la posibilidad de registrar un usuario en la aplicacion.

Despues de la correccion de los errores mencionados por el profesor en la practica anterior, el primer paso que decidi tomar para realizar la practica fue la de añadir la posibilidad de que dados el email y el password de un usuario en un formulario este quedara registrado como usuario de la aplicacion. Para ello, el primer paso era añadir una *evolucion* a la BD de la aplicacion con una nueva tabla *Usuario* donde almacenar los datos anteriormente citados:

**Evolucion - 2.sql**

	#Esquema de usuarios.

	# --- !Ups

	CREATE TABLE Usuario(
		email varchar(255) NOT NULL,
		password varchar(255) NOT NULL,
		PRIMARY KEY(email)
		
	);



	# --- !Downs

	DROP TABLE Usuario;

Siguiendo la convencion de evoluciones de Bases de datos de *Play Framework* he creado un fichero sql con el nombre **2.sql** cuyo contenido arriba refleja que cuando se aplique la evolucion se ha de añadir una tabla al esquema de BBDD y para volver al estado anterior dicha tabla debe borrarse.

Tras adaptar la Base de Datos de la aplicacion habia que implementar la funcionalidad del registro propiamente dicha, para ello, siguiendo el patron MVC que estamos siguiendo durante todo el desarrollo de la aplicacion, tenia que añadir dichos componentes a la aplicacion para implementar la funcionalidad del registro:

**Modelo - User.scala**

	case class User(email: String, password: String)

	object User {

		var user = {
			get[String]("email") ~
			get[String]("password") map {
				case email~password => User(email,password)
			}
		}

		def add(user: User) = {
			DB.withConnection { implicit conn =>
				SQL( "insert into Usuario (email, password) values ({email}, {password})" ).on(
					'email -> user.email,
					'password -> user.password 
				).executeUpdate()				    
			}		
		}7
	}

He añadido una nueva clase al modelo de la aplicacion en el fichero *User.scala* en ella he añido una *Case Class* con los datos que tendra el usuario, un parser para cuando obtengamos los datos de la BD poderlos convertir en una clase de nuestro modelo y un metodo de clase `add(user: User)` que recibido un objeto de la *Case Class* `User` introduce sus datos en la BD.

**Controlador - Application.scala**

	...

	val userForm = Form(
	    mapping(
	      "email" -> email,
	      "password" -> nonEmptyText
	    )(User.apply)(User.unapply)
	)

	...

	//Muestra el formulario para registrar un nuevo usuario.
	def showSigninForm() = Action { implicit request =>
	Ok(views.html.signin(userForm))  
	}

	//Action que registra un nuevo usuario.
	def signNewUser() = Action { implicit request =>
	userForm.bindFromRequest.fold (
	     formWithErrors => BadRequest(views.html.signin(formWithErrors)),
	     user => {
	        User.add(user)
	        Redirect(routes.Application.tasks(order)).withSession("email" -> user.email)
	     }
	 )     
	}

	...

En el controlador principal de la aplicacion, he añadido un nuevo `Form` que sera enviado a la vista para mapear los datos recogidos en ella convirtiendolos en un objeto de la clase `User` del modelo. Tambien, he añadido dos nuevas `Action`.

La primera de ellas mostrara la vista con el formulario de registro de un nuevo usuario, cuando se realize una peticion GET sobre la direccion `/signin` de la apliciacion. La otra, registrara un nuevo usuario validando los campos recogidos en la vista. Y si son correctos, haciendo un *binding* del `Form`, enviara el objeto generado al metodo de clase `add(user: User)` del modelo y redirigira a la vista con las tareas de dicho usuario *logueandolo* en la aplicación (En un principio no se realizaba esta acción puesto que la funcionalidad de login no estaba implementada aun, pero aqui se muestra el codigo final de la aplicacion).

**Vista - signin.scala.html**

	@(userForm : Form[User])

	@import helper._

	@main("Sign new user"){
		<h1>Sign new user</h1>

		@form(routes.Application.signNewUser){
			@inputText(userForm("email"))

			@inputPassword(userForm("password"))

			<input type="submit" value="Sign in">
		}
		<a href="@routes.Application.showLoginForm"> <button>Back</button></a>
		<footer>Created by: Pablo Verdú Romero.</footer>
	}

Por ultimo, la vista unicamente visualiza el formulario solicitando los datos para registrar un nuevo usuario. He empleado el control tipo `inputPassword()` para ocultar lo que el usuario escribe en el formulario. Cuando el usuario haga click en el boton tipo *submit* se invocara la `Action` que desencadena el registro del usuario.


####2.2 Implementando la funcionalidad de Login.

El siguiente incremento que he desarrollado en la aplicacion era permitir que un usuario que, anteriormente, se hubiera registrado en nuestra aplicacion pudiera loguearse accediendo a la vista de tareas (En este momento a la de todos los usuarios, hasta que no se realizaran mas incrementos). Procedere a explicar la implementacion como en el apartado anterior:

**Modelo - User.scala**

	...

	def getUser(email: String) = {
		DB.withConnection { implicit conn =>
		    SQL( "select * from Usuario where email = {email}" ).on(
				'email -> email
			).as(user.singleOpt)
		}
	}

En el modelo, he añadido este metodo de clase que dado un email de un usuario obtiene todos los datos de dicho usuario. El metodo realiza la consulta SQL a la base de datos y utiliza el parseador anteriormente implementado para obtener un objeto del modelo con dichos datos. Se puede observar que he utilizado el metodo de la clase `Anorm` de *Play Framework* `singleOpt` puesto que se trata de una consulta que puede ofrecer un resultado nulo o un unico resultado. En el controlador, aprovechare este forma de parsear los datos para determinar si existe o no el usuario que se intenta loguear.

**Controlador - Application.scala**

	...

	//Muestra el formulario para hacer login.
	def showLoginForm() = Action { implicit request =>
	Ok(views.html.login(userForm, ""))  
	}

	//Realiza el login en la aplicacion comprobando los datos.
	def doLogin() = Action { implicit request =>
	userForm.bindFromRequest.fold (
	  formWithErrors => BadRequest( views.html.login(formWithErrors, "") ),
	  user => {
	    User.getUser(user.email).map{
	      userGet => {
	        if(userGet.password == user.password){
	          Redirect(routes.Application.tasks(order)).withSession("email" -> user.email)
	        } else {
	          BadRequest(views.html.login(userForm, "The password isn't correct"))
	        }
	      }
	    }.getOrElse(BadRequest(views.html.login(userForm, "The user must exist")))
	  }
	)      
	}

	//"Desloguea" al usuario actual.
	def logout() = Action {
	Redirect(routes.Application.showLoginForm).withNewSession
	}

	...

En el controlador he añadido tres nuevas `Action` (Ademas de otras modificaciones que mas adelante detallare). La primera de estas, al igual que en el caso del registro, muestrara la vista con el formulario de login cuando se realize una peticion GET sobre la direccion `/login` de la aplicacion, se puede observar que reutilizo el form para usuario definido anteriormente.

La siguiente `Action`, realiza el login de un usuario procediendo de la siguiente forma: Cuando el usuario invoca esta action (Realiza una peticion POST sobre la direccion `/login`) la action rellena el `Form` de usuario con los datos de la peticion y los valida. Si estos son correctos, se invoca al metodo de del modelo que he explicado antes y, entonces, pueden ocurrir dos cosas. Si el usuario existe, se comprueba que el password de dicho usuario sea identico al introducido en la vista y que se encuentra en el `Form`, si asi es, se redirige la aplicaicon a la vista de tareas añadiendo el email del usuario como variable de Sesion de la aplicacion (Esto es lo que realiza el metodo `withSession("email" -> user.email)` del Redirect), dicha variable se llamara *email* y contendra el dato del email introducido en la vista. Si la contraseña no coincide, se redirigira con una respuesta `BadRequest` a la vista de login añadiendo un parametro de eror a la vista que visualiza que el password no es correcto. Si el usuario no existe, se redirigira a la vista login con una respuesta `BadRequest` añadiendole como parametro de mensaje de error a la vista que dicho usuario no existe. Podemos saber si el usuario existe o no, al *mapear* la respuesta que nos daba el metodo `singleOpt` que hemos añadido en el metodo `getUser(email: String)` del modelo.

La ultima de las `Action` implementadas *"desloguea"* a un usuario activo cuando realiza una peticion GET sobre la direccion `/logout` de la aplicacion enviandolo a la vista login. Para ello utiliza el metodo `withNewSession` de *Play Framework* que borra la sesion actual (Con todas las variables creadas) y crea una nueva.

Como he indicado, tambien he realizado unas modificaciones en el controlador, que resultaban pertinentes despues de haber implementado esta funcionalidad. La primera era cambiar la `Action` *index* que respondia cuando se realizaba una peticion GET sobre la raiz de la aplicacion, mostrando la vista de login y no la de las tareas como mostraba hasta ahora. 

La segunda, era que cuando un usuario se registrara se redirigiera directamente a la vista de tareas con la variable de sesion añadida tambien, como hemos visto que estaba implementado cuando he explicado dicha evolucion de la aplicacion.

**Vista - login.scala.html**

	@(userForm: Form[User], errorMessage: String)
	@import helper._

	@main("Login"){
		<h1>Welcome to MADS TO-DO List</h1>

		<h2>Login</h2>

		<p style="color: red;">@errorMessage</p>

		@form(routes.Application.doLogin){
			@inputText(userForm("email"))
			@inputPassword(userForm("password"))
			<input type="submit" value="Login">
		}
		<a href="@routes.Application.showSigninForm"><button>Sign in new user</button></a>
		<footer>Created by: Pablo Verdú Romero.</footer>
	}

La vista, recibe como parametros el `Form[User]` y un String con el mensaje de error. Implementa los campos de dicho form y si hubiera algun error al realizar el login, lo mostraria en rojo. Tambien he añadido un enlace a la vista de registro de nuevos usuarios, puesto que, esta vista login sera el punto de partida de nuestra aplicacion para cualquier persona que no se haya logueado aun en la misma.

Cabe añadir, que en la vista en la que se listan las tareas, he añadido un enlace a la direccion `logout` para que un usuario logueado pueda *"desloguearse"* correctamente.


####2.3 Identificar las tareas con un usuario concreto.

Este paso ha sido en el que menos he aplicado el espiritu incremental de la practica, puesto que mi idea de como implementarlo ha ido cambiando mientras iba desarrollando el resto de la aplicacion, por lo tanto explicare el resultado final de la implementacion, aunque puede que no coincida con la secuencida de commits realizados.

**Evolucion de la BD - 3.sql**

	# Realacionar una tarea con un usuario.

	# --- !Ups
	ALTER TABLE task ADD COLUMN userEmail varchar(255);

	ALTER TABLE task
		ADD CONSTRAINT fk_task_user
		FOREIGN KEY (userEmail)
		REFERENCES Usuario;  

	# --- !Downs

	ALTER TABLE task
		DROP COLUMN userEmail;

Para empezar, debia implementar una nueva evolucion en la BD para añadir una columna *userEmail* en la tabla de tareas y, tambien, hacerla clave ajena de la tabla *Usuario*, como se refleja en la parte de evolucion del fichero. Para deshacer este incremento en la BD y que volviera a su estado anterior tenia que dejar puesto en el `!Downs` que se borrara esa columna.

**Modelo - Task.scala**

	case class Task(id: Pk[Long], label: String, endDate: Option[Date], user: String)

	...

	//Parseador de tareas al obtenerlas de la BD
	val task = {
		get[Pk[Long]]("id") ~ 
		get[String]("label") ~
		get[Option[Date]]("endDate") ~
		get[String]("userEmail") map {
			case id~label~endDate~user => Task(id,label,endDate,user)
		}
	}

	...

	//Inserta una tarea en la BD asignandole el parametro label como dicho valor.
	def create(task: Task) {
		DB.withConnection { implicit c =>
			SQL("insert into task (label, endDate, userEmail) values ({label}, {endDate}, {userEmail})").on(
				'label -> task.label,
				'endDate -> task.endDate,
				'userEmail -> task.user
			).executeUpdate()
		}
	}

	...

	//Lista las tareas de un usuario
	def listByUser(user: String, order: Option[Int]) : List[Task] = order match {
		case Some(1) => DB.withConnection { implicit c =>
			SQL("""select * from task 
				where userEmail = {userEmail}
				order by endDate""").on(
				'userEmail -> user).as(task *)
		}

		case _ => DB.withConnection { implicit c => 
			SQL("""select * from task 
				where userEmail = {userEmail}
				order by id""").on(
				'userEmail -> user).as(task *) 
		}	
	}

Como se puede observar, en el modelo se han realizado bastantes modificaciones. Para empezar, se ha añadido un campo nuevo en la *Case Class* que define una Tarea en el modelo. Tambien, se ha añadido en el *SqlParser* de *Anorm* `get[String]("userEmail")` para parsear el email correspondiente al usuario de dicha tarea.

El metodo `create(task: Task)` tambien ha sufrido cambios, añadiendo el email del usuario que esta creando la tarea. Y se ha añadido un nuevo metodo `listByUser(user: String, order: Option[Int]) : List[Task]` que lista las tareas dado un email del usuario que las ha creado y el orden (Implementado en la practica anterior).

**Controlador - Application.scala**

	...

	def newTask = getLoggedUser { userEmail => implicit request =>
	    taskForm.bindFromRequest.fold (
	      formWithErrors => BadRequest( views.html.newTask(formWithErrors, userEmail) ),
	      task => {
	          Task.create(task)
	          Redirect(routes.Application.tasks(Application.order))
	      }
	    )
	}

	...

	//Action que responde enviandote a la pagina para crear una nueva tarea.
	def showNewTaskForm = getLoggedUser { userEmail => implicit request =>
		Ok(views.html.newTask(taskForm, userEmail))
	}

	...

	//Funcion que extrae el valor de la variable de sesion "email"
	private def username(request: RequestHeader) = request.session.get("email")

	//Funcion que te redirige a la pagina de login (Para utilizar como callback cuando la autentificacion falla)
	private def onUnauthorized(request: RequestHeader) = BadRequest(views.html.login(userForm, ""))

	//Funcion para añadir la restriccion de que haya un usuario conectado para obtener una action.
	def getLoggedUser(f: => String => Request[AnyContent] => Result) = 
    	Security.Authenticated(username, onUnauthorized) {
      		user => Action(request => f(user)(request))
    	}

En el controlador se han añadido las funciones para manejar la seguridad de la aplicacion que se indicaban en el enunciado de la practica y que he utilizado para obtener la variable de sesion del usuario conectado actualmente y utilizarlo a la hora de añadir una nueva tarea.

La funcion `username(request: RequestHeader)` dada una peticion, obtiene la variable de sesion *"email"* de ella y la devuelve como un String.

`onUnauthorized(request: RequestHeader)` se utilizara como callback se produzca un error en el metodo que comprueba si existe una persona autentificada en la aplicacion y si se invoca te redirigira con una respuesta `BadRequest` a la vista de login.

Por ultimo, la funcion `getLoggedUser(f: => String => Request[AnyContent] => Result)` recibe una funcion *"by-name"* de Scala como parametro, que ademas sera del tipo `A => B => C` de Scala. Esta funcion se invocara en el cuerpo de la funcion y deberemos añadirla cuando invoquemos a *getLoggedUser*. Cuando invocamos a *getLoggedUser*, llama a la funcion de *Play Framework* `Security.Authenticated` que recibe dos funciones como parametro, la primera devuelve un `Option[String]` que sera el email de la variable de sesion. Si este existe, invoca a la action que define como funcion *by-name* cuando se invoca, en nuestro caso generando una funcion que recibe un String como parametro y ejecutando una `Action` que su vez, recibe una `Request` como parametro y ejecuta la funcion *by-name* de *getLoggedUser* que recibe como parametros el String y la Request anteriores y que debe generar un Result completando asi el tipo `String => Request => Result`de Scala, bastante complicado de explicar.

Por tanto, una vez que tenemos estas herramientas, he redefinido la *Action* `showNewTaskForm`del controlador invocando a `getLoggedUser`y definiendo como funcion *by-name* la funcion de tipo `A => B => C` obteniendo en `userEmail` el String, como Request `implicit request` (Es decir la peticion que ha desencadenado a la funcion `showNewTaskForm`) y como Result, redirigiendo con respuesta Ok 200 a la vista para crear una nueva tarea a la cual le paso como parametro el String `userEmail` con el contenido de la variable de sesion "email" del usuario identificado actualmente.

Y en la *Action* `newTask` que crea la tarea, tambien he aplicado `getLoggedUser` para comprobar que en el momento de realizar la peticion POST tambien hay un usuario conectado.

**Vista - newTask.scala.html**

	@(taskForm: Form[Task], userEmail: String)

	@import helper._

	@main("Create new Task"){

		<h1>Add a new task </h1>
		<h2>Hello, @userEmail</h2>

		@form(routes.Application.newTask){
			@inputText(taskForm("label"))

			@inputText(taskForm("endDate"), '_label -> "Date of end")

			<input type="hidden" name="user" value="@userEmail"> 

			<input type="submit" value="Create">
		}
		<footer>Created by: Pablo Verdú Romero.</footer>

	}

La vista, se ha modificado, para que reciba el parametro `userEmail : String` que se introduce en un input de tipo *hidden* para completar el `Form[Task]` y que se cree el objeto del modelo con el dato del usuario que esta creando esta tarea.

Como he indicado antes, este incremento no ha sido todo de una vez puesto que cuando he ido añadiendo otras cosas a la aplicacion se me han ido ocurriendo formas mejores de realizar la inserccion de la tarea. En el commit que describe el evolutivo de la aplicacion con este incremento, se vera otra forma de añadir las tareas.


####2.4 Mostrar el usuario conectado en todas las vistas.

El siguiente incremento que me marque en la planificacion de la practica, era el de mostrar el email del usuario conectado a la aplicacion en las vistas de la misma.

**Modelo - Sin cambios**

No hay cambios en el modelo para implementar esta funcionalidad.


**Controlador - Application.scala**

Para implementar esta funcionalidad aproveche la funcion *getLoggedUser* para obtener la variable de sesion *"email"* del usuario conectado y ir pasandosela como parametro a todas las vistas de la aplicacion, ademas me ayudaba a restringir el acceso a vistas para usuarios registrados a quien no estuviera logueado en la aplicacion. Valga como explicacion de funcionamiento de esta funcion la realizada en el apartado anterior.

**Vista - Todas**

En todas las vistas (En las que se debe estar logueado para acceder) se ha añadido un parametro de tipo String con el email del usuario conectado almacenado en la variable de sesion. Ademas, se ha añadido un pequeño saludo de esta forma:

	<h2>Hello, @userEmail</h2>

Siendo `userEmail` el parametro citado anteriormente.


####2.5 Restringir las operaciones de edicion y borrado de tareas a su propietario.

El ultimo incremento a realizar en la aplicacion era restringir el acceso a las operaciones de edicion y borrado de una tarea al usuario que la habia creado anteriormente. Procedo a explicar los cambios de la aplicacion:

**Modelo - Task.scala**

	//Devuelve true si la tarea con id pasado por parametro corresponde con el usuario pasado por parametro,
	//y false si no.
	def isOwner(id: Long, email: String) : Boolean = {
		val result = DB.withConnection { implicit conn =>
	    	SQL( "select userEmail from task where id={id}" ).on(
		    	'id -> id).as(scalar[String].singleOpt)
		}
		if(result == None || result.get != email){
			return false
		} else {
			return true
		}
	}

En el modelo he añadido un nuevo metodo que dados el id de una tarea y el email del usuario que creo la misma, determina si dicha tarea peternece a dicho usuario o no. Como la consulta devolvia un String, he tenido que parsearla con el metodo `scalar[T]` de *Anorm* y, a su vez, aplicarle el metodo `singleOpt` puesto que la consulta podia ofrecer un resultado o ninguno. Una vez la realizaba, si la consulta no ofrecia ningun resultado (La tarea no existia) o el email no era el mismo de la del usuario que intentaba acceder a ella se devolvia `false`. Si no era asi, se devolvia `true`.

**Controlador - Application.scala**

	...

	//Action que borra la tarea pasada por parametro y te rederige a la lista de tareas.
	def deleteTask(id: Long) = IsOwnerOf(id){ userEmail => implicit request =>
		Task.delete(id)
		Redirect(routes.Application.tasks(Application.order))    
	}

	...


	//Action que responde enviandote a la pagina para editar una nueva tarea.
	def editTask(id: Long) = IsOwnerOf(id) { userEmail => implicit request =>
    	Task.findById(id).map { task =>
      	Ok(views.html.editTask(id, taskForm.fill(task), userEmail))
    	}.getOrElse(NotFound("Task not found"))
	}
  
	//Action que actualiza la informacion de una tarea.
	def updateTask(id: Long) = IsOwnerOf(id) { userEmail => implicit request =>
    	taskForm.bindFromRequest.fold (
      		formWithErrors => BadRequest(views.html.editTask(id, formWithErrors, userEmail)),
      		task => {
          		Task.update(id, task)
          		Redirect(routes.Application.tasks(Application.order))
	     	}
	    )
	}

	...

	//Funcion para añadir la restriccion de que un recurso sea propiedad de un usuario.
	def IsOwnerOf(task: Long)(f: => String => Request[AnyContent] => Result) = 
	getLoggedUser { user => request =>
      if(Task.isOwner(task, user)) {
         f(user)(request)
      } else {
         Results.Forbidden("You aren't authorized")
      }
	}

En el controlador he utilizado como se puede observar la funcion `IsOwnerTask(task: Long)(f: => String => Request[AnyContent] => Result)` que se indicaba en el enunciado de la practica. Esta funcion determina, dado el id de una tarea, si esta pertenece al usuario conectado actualmente ejecutando una funcion *by-name* si asi es. Para ello, utiliza a la funcion *getLoggedUser* que determina el usuario conectado actualmente, y ejecuta el metodo del modelo explicado en el punto enterior pasandole los parametros id de la tarea y usuario conectado. Si la tarea es de dicho usuario se ejecuta la funcion *by-name* pasandole como parametros las funciones `user` y `request` que se obtienen de *getLoggedUser* y que debera ser implementada en el momento de invocar a `IsOwnerTask` teniendo que devolver un *Result* para completar el tipo `String => Request => Result`. Si la tarea no es de dicho usuario se completa el *Result* de *getLoggedUser* con una respuesta `Forbbiden` a la que se le pasa como parametro una cadena para indicar al usuario que no esta identificado.

Una vez explicada esta funcion, decir que se aplica a las *Action* que muestra el formulario de editar una tarea, realizan la edicion de la tarea y borran la tarea. Asi, se comprueba que cuando un usuario intenta acceder a dichas funciones, es el usuario que creo la tarea sobre la que pretende realizar la accion.

**Vista - editTask.scala.html**

En dicha vista se realiza una modificacion a la hora de rellenar el `Form[User]` de la tarea que se esta editando, que es la misma modificacion que he realizado en la vista para crear una nueva tarea añadir un input de tipo *hidden* con el valor del usuario conectado a la aplicacion, puesto que el propietario de la tarea no se puede modificar:

	<input type="hidden" name="user" value="@userEmail">

Con esto, queda concluida la explicacion del desarrollo incremental de la practica.

###3. Nuevas caracteristicas Play utilizadas.

En esta practica hemos utilizado como nuevas caracteristicas de Play, las *evolutions* de la Base de datos, la utilizacion de variables de sesion con el metodo `withSession` o la creacion de una nueva sesion con el metodo `withNewSession` y el metodo de seguridad `Security.Authenticated`.

Tambien hemos aprendido conceptos nuevos de Scala como son las funciones *by-name* y el tipo *A => B => C*.

Creo que todo ello ha sido explicado con bastante detalle en el apartado anterior cuando han ido surgiendo.

Resaltar que para comprender estos conceptos he utilizado como bibliografia los siguientes enlaces:

-[Documentacion de Anorm](http://www.playframework.com/documentation/2.2.x/ScalaAnorm): para entender mejor las evoluciones de la BD.

-[Documentacion de Sesiones](http://www.playframework.com/documentation/2.0/ScalaSessionFlash): para aclarar el manejo de sesiones en *Play Framework*

-[Api Security de Play](http://www.playframework.com/documentation/2.0/api/scala/index.html#play.api.mvc.Security$): para comprender el uso del metodo `Security.Authenticated`.  